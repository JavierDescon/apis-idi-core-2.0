﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Xml;
using System.Xml.Linq;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.ComponentModel.DataAnnotations;

namespace APIs.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    [DataContract(Namespace = "")]
    [XmlRoot("Request")]
    public class XmlController : ControllerBase
    {

        string nodos;
        [HttpGet]
        public string Activa()
        {
            return "Funciona xml";
        }

        [HttpPost]
        public string GetDatosXML([FromBody] XElement xml)
        {
            nodos = "";
            XmlDocument documento = new XmlDocument();
            documento.LoadXml(xml.ToString());
            for (int i = 0; i < documento.ChildNodes.Count; i++)
            {
                nodos += documento.ChildNodes[i].Name + "{" + System.Environment.NewLine;
                obtenerAtributos(documento.ChildNodes[i]);
                obtenerNodos(documento.ChildNodes[i]);
                nodos += "}";
            }
            return "se ha procesado el XML";
        }



        //public string Get([FromForm] XmlDocument xml)
        //{
        //    nodos = "";
        //    XmlDocument documento = new XmlDocument();
        //    documento.LoadXml(xml.ToString());
        //    for (int i = 0; i < documento.ChildNodes.Count; i++)
        //    {
        //        nodos += documento.ChildNodes[i].Name + "{" + System.Environment.NewLine;
        //        obtenerAtributos(documento.ChildNodes[i]);
        //        obtenerNodos(documento.ChildNodes[i]);
        //        nodos += "}";
        //    }
        //    return nodos;
        //}

        private void obtenerNodos(XmlNode nodo){
            if (nodo.ChildNodes.Count>0)
            {
                for (int i = 0; i < nodo.ChildNodes.Count; i++)
                {
                    nodos +="-> "+ nodo.ChildNodes[i].Name;
                    obtenerAtributos(nodo.ChildNodes[i]);
                    nodos += "{" ;
                    obtenerNodos(nodo.ChildNodes[i]);
                    nodos += "}";
                }
            }
            else
            {
                if ( nodo.Value!=null)
                {
                    nodos +="->"+ nodo.Value;
                }
                nodos += System.Environment.NewLine;
            }
        }
        private void obtenerAtributos(XmlNode nodo) {
            if (nodo.Attributes !=null)
            {
                nodos += "(";
                for (int i = 0; i < nodo.Attributes.Count; i++)
                {
                    nodos += nodo.Attributes[i].Name + "=" + nodo.Attributes[i].Value +", ";
                }
                nodos += ")";
            }
        }
    }
}