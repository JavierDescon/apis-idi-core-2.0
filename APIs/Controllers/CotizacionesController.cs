﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using APIs.Models;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;

using SAPbobsCOM;

namespace APIs.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CotizacionesController : ControllerBase
    {
       
        private readonly IConfiguration _configuration;
        List<CotizacionPersonalizada> listaCotizaciones;

        public CotizacionesController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet]
        public IEnumerable<CotizacionPersonalizada> getCotizaciones()

        {
            string server = _configuration["Configuration:server"];
            string user = _configuration["Configuration:user"];
            string bd = _configuration["Configuration:database"];
            string pass = _configuration["Configuration:pass"];

            listaCotizaciones = new List<CotizacionPersonalizada>();
           
            SqlCommand query = new SqlCommand("SELECT DocEntry,DocNum,CardName,Doctotal,DocDate FROM OQUT INNER JOIN OSLP On OQUT.SlpCode = OSLP.SlpCode WHERE OSLP.SlpName  IS NOT NULL; ", ConexionBd.Conectar(server, bd, user, pass));
            SqlDataReader read = query.ExecuteReader();
            while (read.Read())
            {
                CotizacionPersonalizada cotizacionTemp = new CotizacionPersonalizada();
                cotizacionTemp.DocEntry = read[0].ToString();
                cotizacionTemp.DocNum = read[1].ToString();
                cotizacionTemp.CardName = read[2].ToString();
                cotizacionTemp.DocTotal =read[3].ToString();
                cotizacionTemp.DocDate = read[4].ToString();
                cotizacionTemp.detalles = getDetalles(cotizacionTemp);
                listaCotizaciones.Add(cotizacionTemp);
            }
            read.Close();
            return listaCotizaciones;
        }

     

        // GET api/cotizaciones/5
        [HttpGet("{SlpCode}")]
        public IEnumerable<CotizacionPersonalizada> getCotizaciones(int SlpCode) // listado de cotizaciones por vendedor 
        {
            //_configuration = configuration;
            string server = _configuration["Configuration:server"];
            string user = _configuration["Configuration:user"];
            string bd = _configuration["Configuration:database"];
            string pass = _configuration["Configuration:pass"];

            List<CotizacionPersonalizada> listaCotizaciones = new List<CotizacionPersonalizada>();
            SqlCommand query = new SqlCommand("SELECT T1.DocNum, T1.CardCode, T1.CardName " +
                "fROM OQUT T1 INNER JOIN CRD1 T2 ON  T1.ShipToCode = T2.Address " +
                "WHERE T1.DocStatus = 'O' AND T1.SlpCode ='"+SlpCode+"'", ConexionBd.Conectar(server, bd, user, pass));
            SqlDataReader read = query.ExecuteReader();
            while (read.Read())
            {
                CotizacionPersonalizada cotizacionTemp = new CotizacionPersonalizada();
                
                cotizacionTemp.DocNum = read[0].ToString();
                cotizacionTemp.CardCode = read[1].ToString();
                cotizacionTemp.CardName = read[2].ToString();
                cotizacionTemp.DocEntry = cotizacionTemp.DocNum;
                cotizacionTemp.detalles = getDetalles(cotizacionTemp);
                listaCotizaciones.Add(cotizacionTemp);
            }
            return listaCotizaciones;
        }

        public string sErrMsg;
        public int lErrCode;
        public int lRetCode;
        public Boolean Conectado = false;
        public Company oCompany;
        
        public void ConectaSAP()
        {
            try
            {
                string server = _configuration["Configuration:server"];
                string user = _configuration["Configuration:user"];
                string bd = _configuration["Configuration:database"];
                string pass = _configuration["Configuration:pass"];
                string LicenseServer = _configuration["Configuration:LicenseServer"];
                string DbUserName = _configuration["Configuration:DbUserName"];
                string DbPassword = _configuration["Configuration:DbPassword"];
                string TipoSQL = _configuration["Configuration:TipoSQL"];


                oCompany = new Company();

                oCompany.Server = server;
                oCompany.LicenseServer = LicenseServer;
                oCompany.DbUserName = user;
                oCompany.DbPassword = pass;

                if (TipoSQL == "2008")
                {
                    oCompany.DbServerType = (SAPbobsCOM.BoDataServerTypes.dst_MSSQL2008);
                }
                else if (TipoSQL == "2012")
                {
                    oCompany.DbServerType = (SAPbobsCOM.BoDataServerTypes.dst_MSSQL2012);
                }
                else if (TipoSQL == "2014")
                {
                    oCompany.DbServerType = (SAPbobsCOM.BoDataServerTypes.dst_MSSQL2014);
                }
                else if (TipoSQL == "2016")
                {
                    oCompany.DbServerType = (SAPbobsCOM.BoDataServerTypes.dst_MSSQL2016);
                }

                oCompany.UseTrusted = false;
                oCompany.CompanyDB = bd;
                oCompany.UserName = DbUserName;
                oCompany.Password = DbPassword;

                // Connecting to a company DB
                lRetCode = oCompany.Connect();

                if (lRetCode != 0)
                {
                    int temp_int = lErrCode;
                    string temp_string = sErrMsg;
                    oCompany.GetLastError(out temp_int, out temp_string);
                    //MessageBox.Show(globals_Renamed.sErrMsg);
                }
                else
                {

                    Conectado = true;
                    // Disable controls

                }

            }
            catch (Exception ex)
            {

            }
            
        }

        [HttpPost]
        public ActionResult Insertar(Cotizaciones cotizacion)
        {
            Cotizaciones co = new Cotizaciones();
            JsonResult result = new JsonResult(co);
            co.SlpCode = "1saf";
            string s = result.ToString();

            string DocEntry = cotizacion.DocEntry;
            string DocNum = cotizacion.DocNum;
            string CardName = cotizacion.CardName;
            string DocTotal = cotizacion.DocTotal;
            string DocDate = cotizacion.DocDate;
            string CardCode = cotizacion.CardCode;
            int SlpCode = Convert.ToInt16( cotizacion.SlpCode);

            if (Conectado == false)
            {
                ConectaSAP();
            }

            SAPbobsCOM.Documents OCotizacion;
            OCotizacion = (SAPbobsCOM.Documents)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oQuotations);
            //INformación de Encabezado
            //OCotizacion.UserFields.Fields.Item("U_Pedido").Value = docNum2;
            OCotizacion.CardCode = CardCode;
            OCotizacion.DocDate = Convert.ToDateTime(DocDate);
            OCotizacion.SalesPersonCode = SlpCode;
            OCotizacion.NumAtCard = "Cotización creada desde el Portal";

            //Aqui creamos las lineas
            foreach (var linea in cotizacion.lineas)
            {
                string ItemCode = linea.ItemCode;
                double quantity = linea.quantity;
                double price = linea.price;
                OCotizacion.Lines.TaxCode = "B16";
                OCotizacion.Lines.ItemCode = ItemCode;
                OCotizacion.Lines.Quantity = quantity;
                OCotizacion.Lines.Price = price;
                OCotizacion.Lines.Add();
            }
            //foreach (GridViewRow row in dgvCotizaciones.Rows)
            //{
            //    TextBox txt2 = (TextBox)(row.FindControl("txtQuantity"));
            //    ItemCode = row.Cells[0].Text;
            //    Price = Convert.ToDecimal(row.Cells[2].Text);
            //    Quantity = Convert.ToDecimal(txt2.Text);
            //    OCotizacion.Lines.TaxCode = "B16";
            //    OCotizacion.Lines.ItemCode = ItemCode;
            //    OCotizacion.Lines.Quantity = Convert.ToDouble(Quantity);
            //    OCotizacion.Lines.Price = Convert.ToDouble(Price);
            //    OCotizacion.Lines.Add();
            //}
            
            //Agregar Documento 
            lErrCode = OCotizacion.Add();

            if (lErrCode != 0)
            {
                int temp_int = lErrCode;
                string temp_string = sErrMsg;
                oCompany.GetLastError(out temp_int, out temp_string);
                return NotFound(temp_string);
               ;
                //Poner mensaje de error que lea esto
                //resultado = temp_string;
                //MessageBox.Show(temp_string);
            }


            else
            {
                //Aqui cuando se crea la APS correctamente
                // = "APS was added successfully";

                oCompany.Disconnect();
                return Ok("Registro insertado");
                //mostrar_Mensaje("cotización creada satisfactoriamente");

            }

            
        }


        private Detalles getDetalles(CotizacionPersonalizada cotizacion)
        {
            string server = _configuration["Configuration:server"];
            string user = _configuration["Configuration:user"];
            string bd = _configuration["Configuration:database"];
            string pass = _configuration["Configuration:pass"];
            SqlConnection conexion = ConexionBd.Conectar(server, bd, user, pass);
            Detalles detalles = new Detalles();
            SqlCommand query = new SqlCommand("SELECT * FROM OQUT Where DocEntry='" + cotizacion.DocNum + "';", conexion);
            SqlDataReader read = query.ExecuteReader();
            while (read.Read())
            {

                detalles.DocType = read["DocType"].ToString();
                detalles.CANCELED = read["CANCELED"].ToString();
                detalles.Handwrtten = read["Handwrtten"].ToString();
                detalles.Printed = read["Printed"].ToString();
                detalles.DocStatus = read["DocStatus"].ToString();
                detalles.InvntSttus = read["InvntSttus"].ToString();
                detalles.Transfered = read["Transfered"].ToString();
                detalles.ObjType = read["ObjType"].ToString();
                detalles.DocDueDate = read["DocDueDate"].ToString();
                detalles.CardCode = read["CardCode"].ToString();
                detalles.Address = read["Address"].ToString();
                detalles.NumAtCard = read["NumAtCard"].ToString();
                detalles.VatPercent = read["VatPercent"].ToString();
                detalles.VatSum = read["VatSum"].ToString();
                detalles.VatSumFC = read["VatSumFC"].ToString();
                detalles.DiscPrcnt = read["DiscPrcnt"].ToString();
                detalles.DiscSum = read["DiscSum"].ToString();
                detalles.DiscSumFC = read["DiscSumFC"].ToString();
                detalles.DocCur = read["DocCur"].ToString();
                detalles.DocRate = read["DocRate"].ToString();
                detalles.DocTotalFC = read["DocTotalFC"].ToString();
                detalles.PaidToDate = read["PaidToDate"].ToString();
                detalles.PaidFC = read["PaidFC"].ToString();
                detalles.GrosProfit = read["GrosProfit"].ToString();
                detalles.GrosProfFC = read["GrosProfFC"].ToString();
                detalles.Comments = read["Comments"].ToString();
                detalles.JrnlMemo = read["JrnlMemo"].ToString();
                detalles.TransId = read["TransId"].ToString();
                detalles.ReceiptNum = read["ReceiptNum"].ToString();
                detalles.GroupNum = read["GroupNum"].ToString();
                detalles.DocTime = read["DocTime"].ToString();
                detalles.SlpCode = read["SlpCode"].ToString();
                detalles.TrnspCode = read["TrnspCode"].ToString();
                detalles.PartSupply = read["PartSupply"].ToString();
                detalles.Confirmed = read["Confirmed"].ToString();
                detalles.GrossBase = read["GrossBase"].ToString();
                detalles.ImportEnt = read["ImportEnt"].ToString();
                detalles.CreateTran = read["CreateTran"].ToString();
                detalles.SummryType = read["SummryType"].ToString();
                detalles.UpdInvnt = read["UpdInvnt"].ToString();
                detalles.UpdCardBal = read["UpdCardBal"].ToString();
                detalles.Instance = read["Instance"].ToString();
                detalles.Flags = read["Flags"].ToString();
                detalles.InvntDirec = read["InvntDirec"].ToString();
                detalles.CntctCode = read["CntctCode"].ToString();
                detalles.ShowSCN = read["ShowSCN"].ToString();
                detalles.FatherCard = read["FatherCard"].ToString();
                detalles.SysRate = read["SysRate"].ToString();
                detalles.CurSource = read["CurSource"].ToString();
                detalles.VatSumSy = read["VatSumSy"].ToString();
                detalles.DiscSumSy = read["DiscSumSy"].ToString();
                detalles.DocTotalSy = read["DocTotalSy"].ToString();
                detalles.PaidSys = read["PaidSys"].ToString();
                detalles.FatherType = read["FatherType"].ToString();
                detalles.GrosProfSy = read["GrosProfSy"].ToString();
                detalles.UpdateDate = read["UpdateDate"].ToString();
                detalles.IsICT = read["IsICT"].ToString();
                detalles.CreateDate = read["CreateDate"].ToString();
                detalles.Volume = read["Volume"].ToString();
                detalles.VolUnit = read["VolUnit"].ToString();
                detalles.Weight = read["Weight"].ToString();
                detalles.WeightUnit = read["WeightUnit"].ToString();
                detalles.Series = read["Series"].ToString();
                detalles.TaxDate = read["TaxDate"].ToString();
                detalles.Filler = read["Filler"].ToString();
                detalles.DataSource = read["DataSource"].ToString();
                detalles.StampNum = read["StampNum"].ToString();
                detalles.isCrin = read["isCrin"].ToString();
                detalles.FinncPriod = read["FinncPriod"].ToString();
                detalles.UserSign = read["UserSign"].ToString();
                detalles.selfInv = read["selfInv"].ToString();
                detalles.VatPaid = read["VatPaid"].ToString();
                detalles.VatPaidFC = read["VatPaidFC"].ToString();
                detalles.VatPaidSys = read["VatPaidSys"].ToString();
                detalles.UserSign2 = read["UserSign2"].ToString();
                detalles.WddStatus = read["WddStatus"].ToString();
                detalles.draftKey = read["draftKey"].ToString();
                detalles.TotalExpns = read["TotalExpns"].ToString();
                detalles.TotalExpFC = read["TotalExpFC"].ToString();
                detalles.TotalExpSC = read["TotalExpSC"].ToString();
                detalles.DunnLevel = read["DunnLevel"].ToString();
                detalles.Address2 = read["Address2"].ToString();
                detalles.LogInstanc = read["LogInstanc"].ToString();
                detalles.Exported = read["Exported"].ToString();
                detalles.StationID = read["StationID"].ToString();
                detalles.Indicator = read["Indicator"].ToString();
                detalles.NetProc = read["NetProc"].ToString();
                detalles.AqcsTax = read["AqcsTax"].ToString();
                detalles.AqcsTaxFC = read["AqcsTaxFC"].ToString();
                detalles.AqcsTaxSC = read["AqcsTaxSC"].ToString();
                detalles.CashDiscPr = read["CashDiscPr"].ToString();
                detalles.CashDiscnt = read["CashDiscnt"].ToString();
                detalles.CashDiscFC = read["CashDiscFC"].ToString();
                detalles.CashDiscSC = read["CashDiscSC"].ToString();
                detalles.ShipToCode = read["ShipToCode"].ToString();
                detalles.LicTradNum = read["LicTradNum"].ToString();
                detalles.PaymentRef = read["PaymentRef"].ToString();
                detalles.WTSum = read["WTSum"].ToString();
                detalles.WTSumFC = read["WTSumFC"].ToString();
                detalles.WTSumSC = read["WTSumSC"].ToString();
                detalles.RoundDif = read["RoundDif"].ToString();
                detalles.RoundDifFC = read["RoundDifFC"].ToString();
                detalles.RoundDifSy = read["RoundDifSy"].ToString();
                detalles.CheckDigit = read["CheckDigit"].ToString();
                detalles.Form1099 = read["Form1099"].ToString();
                detalles.Box1099 = read["Box1099"].ToString();
                detalles.submitted = read["submitted"].ToString();
                detalles.PoPrss = read["PoPrss"].ToString();
                detalles.Rounding = read["Rounding"].ToString();
                detalles.RevisionPo = read["RevisionPo"].ToString();
                detalles.Segment = read["Segment"].ToString();
                detalles.ReqDate = read["ReqDate"].ToString();
                detalles.CancelDate = read["CancelDate"].ToString();
                detalles.PickStatus = read["PickStatus"].ToString();
                detalles.Pick = read["Pick"].ToString();
                detalles.BlockDunn = read["BlockDunn"].ToString();
                detalles.PeyMethod = read["PeyMethod"].ToString();
                detalles.PayBlock = read["PayBlock"].ToString();
                detalles.PayBlckRef = read["PayBlckRef"].ToString();
                detalles.MaxDscn = read["MaxDscn"].ToString();
                detalles.Reserve = read["Reserve"].ToString();
                detalles.Max1099 = read["Max1099"].ToString();
                detalles.CntrlBnk = read["CntrlBnk"].ToString();
                detalles.PickRmrk = read["PickRmrk"].ToString();
                detalles.ISRCodLine = read["ISRCodLine"].ToString();
                detalles.ExpAppl = read["ExpAppl"].ToString();
                detalles.ExpApplFC = read["ExpApplFC"].ToString();
                detalles.ExpApplSC = read["ExpApplSC"].ToString();
                detalles.Project = read["Project"].ToString();
                detalles.DeferrTax = read["DeferrTax"].ToString();
                detalles.LetterNum = read["LetterNum"].ToString();
                detalles.FromDate = read["FromDate"].ToString();
                detalles.ToDate = read["ToDate"].ToString();
                detalles.WTApplied = read["WTApplied"].ToString();
                detalles.WTAppliedF = read["WTAppliedF"].ToString();
                detalles.BoeReserev = read["BoeReserev"].ToString();
                detalles.AgentCode = read["AgentCode"].ToString();
                detalles.WTAppliedS = read["WTAppliedS"].ToString();
                detalles.EquVatSum = read["EquVatSum"].ToString();
                detalles.EquVatSumF = read["EquVatSumF"].ToString();
                detalles.EquVatSumS = read["EquVatSumS"].ToString();
                detalles.Installmnt = read["Installmnt"].ToString();
                detalles.VATFirst = read["VATFirst"].ToString();
                detalles.NnSbAmnt = read["NnSbAmntSC"].ToString();
                detalles.NnSbAmntSC = read["NbSbAmntFC"].ToString();
                detalles.NbSbAmntFC = read["ExepAmnt"].ToString();
                detalles.ExepAmnt = read["ExepAmntSC"].ToString();
                detalles.ExepAmntSC = read["ExepAmntFC"].ToString();
                detalles.ExepAmntFC = read["VatDate"].ToString();
                detalles.VatDate = read["VatDate"].ToString();
                detalles.CorrExt = read["CorrExt"].ToString();
                detalles.CorrInv = read["CorrInv"].ToString();
                detalles.NCorrInv = read["NCorrInv"].ToString();
                detalles.CEECFlag = read["CEECFlag"].ToString();
                detalles.BaseAmnt = read["BaseAmnt"].ToString();
                detalles.BaseAmntSC = read["BaseAmntSC"].ToString();
                detalles.BaseAmntFC = read["BaseAmntFC"].ToString();
                detalles.CtlAccount = read["CtlAccount"].ToString();
                detalles.BPLId = read["BPLId"].ToString();
                detalles.BPLName = read["BPLName"].ToString();
                detalles.VATRegNum = read["VATRegNum"].ToString();
                detalles.TxInvRptNo = read["TxInvRptNo"].ToString();
                detalles.TxInvRptDt = read["TxInvRptDt"].ToString();
                detalles.KVVATCode = read["KVVATCode"].ToString();
                detalles.WTDetails = read["WTDetails"].ToString();
                detalles.SumAbsId = read["SumAbsId"].ToString();
                detalles.SumRptDate = read["SumRptDate"].ToString();
                detalles.PIndicator = read["PIndicator"].ToString();
                detalles.ManualNum = read["ManualNum"].ToString();
                detalles.UseShpdGd = read["UseShpdGd"].ToString();
                detalles.BaseVtAt = read["BaseVtAt"].ToString();
                detalles.BaseVtAtSC = read["BaseVtAtSC"].ToString();
                detalles.BaseVtAtFC = read["BaseVtAtFC"].ToString();
                detalles.NnSbVAt = read["NnSbVAt"].ToString();
                detalles.NnSbVAtSC = read["NnSbVAtSC"].ToString();
                detalles.NbSbVAtFC = read["NbSbVAtFC"].ToString();
                detalles.ExptVAt = read["ExptVAt"].ToString();
                detalles.ExptVAtSC = read["ExptVAtSC"].ToString();
                detalles.ExptVAtFC = read["ExptVAtFC"].ToString();
                detalles.LYPmtAt = read["LYPmtAt"].ToString();
                detalles.LYPmtAtSC = read["LYPmtAtSC"].ToString();
                detalles.LYPmtAtFC = read["LYPmtAtFC"].ToString();
                detalles.ExpAnSum = read["ExpAnSum"].ToString();
                detalles.ExpAnSys = read["ExpAnSys"].ToString();
                detalles.ExpAnFrgn = read["ExpAnFrgn"].ToString();
                detalles.DocSubType = read["DocSubType"].ToString();
                detalles.DpmStatus = read["DpmStatus"].ToString();
                detalles.DpmAmnt = read["DpmAmnt"].ToString();
                detalles.DpmAmntSC = read["DpmAmntSC"].ToString();
                detalles.DpmAmntFC = read["DpmAmntFC"].ToString();
                detalles.DpmDrawn = read["DpmDrawn"].ToString();
                detalles.DpmPrcnt = read["DpmPrcnt"].ToString();
                detalles.PaidSum = read["PaidSum"].ToString();
                detalles.PaidSumFc = read["PaidSumFc"].ToString();
                detalles.PaidSumSc = read["PaidSumSc"].ToString();
                detalles.FolioPref = read["FolioPref"].ToString();
                detalles.FolioNum = read["FolioNum"].ToString();
                detalles.DpmAppl = read["DpmAppl"].ToString();
                detalles.DpmApplFc = read["DpmApplFc"].ToString();
                detalles.DpmApplSc = read["DpmApplSc"].ToString();
                detalles.LPgFolioN = read["LPgFolioN"].ToString();
                detalles.Header = read["Header"].ToString();
                detalles.Footer = read["Footer"].ToString();
                detalles.Posted = read["Posted"].ToString();
                detalles.OwnerCode = read["OwnerCode"].ToString();
                detalles.BPChCode = read["BPChCode"].ToString();
                detalles.BPChCntc = read["BPChCntc"].ToString();
                detalles.PayToCode = read["PayToCode"].ToString();
                detalles.IsPaytoBnk = read["IsPaytoBnk"].ToString();
                detalles.BnkCntry = read["BnkCntry"].ToString();
                detalles.BankCode = read["BankCode"].ToString();
                detalles.BnkAccount = read["BnkAccount"].ToString();
                detalles.BnkBranch = read["BnkBranch"].ToString();
                detalles.isIns = read["isIns"].ToString();
                detalles.TrackNo = read["TrackNo"].ToString();
                detalles.VersionNum = read["VersionNum"].ToString();
                detalles.LangCode = read["LangCode"].ToString();
                detalles.BPNameOW = read["BPNameOW"].ToString();
                detalles.BillToOW = read["BillToOW"].ToString();
                detalles.ShipToOW = read["ShipToOW"].ToString();
                detalles.RetInvoice = read["RetInvoice"].ToString();
                detalles.ClsDate = read["ClsDate"].ToString();
                detalles.MInvNum = read["MInvNum"].ToString();
                detalles.MInvDate = read["MInvDate"].ToString();
                detalles.SeqCode = read["SeqCode"].ToString();
                detalles.Serial = read["Serial"].ToString();
                detalles.SeriesStr = read["SeriesStr"].ToString();
                detalles.SubStr = read["SubStr"].ToString();
                detalles.Model = read["Model"].ToString();
                detalles.TaxOnExp = read["TaxOnExp"].ToString();
                detalles.TaxOnExpFc = read["TaxOnExpFc"].ToString();
                detalles.TaxOnExpSc = read["TaxOnExpSc"].ToString();
                detalles.TaxOnExAp = read["TaxOnExAp"].ToString();
                detalles.TaxOnExApF = read["TaxOnExApF"].ToString();
                detalles.TaxOnExApS = read["TaxOnExApS"].ToString();
                detalles.LastPmnTyp = read["LastPmnTyp"].ToString();
                detalles.LndCstNum = read["LndCstNum"].ToString();
                detalles.UseCorrVat = read["UseCorrVat"].ToString();
                detalles.BlkCredMmo = read["BlkCredMmo"].ToString();
                detalles.OpenForLaC = read["OpenForLaC"].ToString();
                detalles.Excised = read["Excised"].ToString();
                detalles.ExcRefDate = read["ExcRefDate"].ToString();
                detalles.ExcRmvTime = read["ExcRmvTime"].ToString();
                detalles.SrvGpPrcnt = read["SrvGpPrcnt"].ToString();
                detalles.DepositNum = read["DepositNum"].ToString();
                detalles.CertNum = read["CertNum"].ToString();
                detalles.DutyStatus = read["DutyStatus"].ToString();
                detalles.AutoCrtFlw = read["AutoCrtFlw"].ToString();
                detalles.FlwRefDate = read["FlwRefDate"].ToString();
                detalles.FlwRefNum = read["FlwRefNum"].ToString();
                detalles.VatJENum = read["VatJENum"].ToString();
                detalles.DpmVat = read["DpmVat"].ToString();
                detalles.DpmVatFc = read["DpmVatFc"].ToString();
                detalles.DpmVatSc = read["DpmVatSc"].ToString();
                detalles.DpmAppVat = read["DpmAppVat"].ToString();
                detalles.DpmAppVatF = read["DpmAppVatF"].ToString();
                detalles.DpmAppVatS = read["DpmAppVatS"].ToString();
                detalles.InsurOp347 = read["InsurOp347"].ToString();
                detalles.IgnRelDoc = read["IgnRelDoc"].ToString();
                detalles.BuildDesc = read["BuildDesc"].ToString();
                detalles.ResidenNum = read["ResidenNum"].ToString();
                detalles.Checker = read["Checker"].ToString();
                detalles.Payee = read["Payee"].ToString();
                detalles.CopyNumber = read["CopyNumber"].ToString();
                detalles.SSIExmpt = read["SSIExmpt"].ToString();
                detalles.PQTGrpSer = read["PQTGrpSer"].ToString();
                detalles.PQTGrpNum = read["PQTGrpNum"].ToString();
                detalles.PQTGrpHW = read["PQTGrpHW"].ToString();
                detalles.ReopOriDoc = read["ReopOriDoc"].ToString();
                detalles.ReopManCls = read["ReopManCls"].ToString();
                detalles.DocManClsd = read["DocManClsd"].ToString();
                detalles.ClosingOpt = read["ClosingOpt"].ToString();
                detalles.SpecDate = read["SpecDate"].ToString();
                detalles.Ordered = read["Ordered"].ToString();
                detalles.NTSApprov = read["NTSApprov"].ToString();
                detalles.NTSWebSite = read["NTSWebSite"].ToString();
                detalles.NTSeTaxNo = read["NTSeTaxNo"].ToString();
                detalles.NTSApprNo = read["NTSApprNo"].ToString();
                detalles.PayDuMonth = read["PayDuMonth"].ToString();
                detalles.ExtraMonth = read["ExtraMonth"].ToString();
                detalles.ExtraDays = read["ExtraDays"].ToString();
                detalles.CdcOffset = read["CdcOffset"].ToString();
                detalles.SignMsg = read["SignMsg"].ToString();
                detalles.SignDigest = read["SignDigest"].ToString();
                detalles.CertifNum = read["CertifNum"].ToString();
                detalles.KeyVersion = read["KeyVersion"].ToString();
                detalles.EDocGenTyp = read["EDocGenTyp"].ToString();
                detalles.ESeries = read["ESeries"].ToString();
                detalles.EDocNum = read["EDocNum"].ToString();
                detalles.EDocExpFrm = read["EDocExpFrm"].ToString();
                detalles.OnlineQuo = read["OnlineQuo"].ToString();
                detalles.POSEqNum = read["POSEqNum"].ToString();
                detalles.POSManufSN = read["POSManufSN"].ToString();
                detalles.POSCashN = read["POSCashN"].ToString();
                detalles.EDocStatus = read["EDocStatus"].ToString();
                detalles.EDocCntnt = read["EDocCntnt"].ToString();
                detalles.EDocProces = read["EDocProces"].ToString();
                detalles.EDocErrCod = read["EDocErrCod"].ToString();
                detalles.EDocErrMsg = read["EDocErrMsg"].ToString();
                detalles.EDocCancel = read["EDocCancel"].ToString();
                detalles.EDocTest = read["EDocTest"].ToString();
                detalles.EDocPrefix = read["EDocPrefix"].ToString();
                detalles.CUP = read["CUP"].ToString();
                detalles.CIG = read["CIG"].ToString();
                detalles.DpmAsDscnt = read["DpmAsDscnt"].ToString();
                detalles.Attachment = read["Attachment"].ToString();
                detalles.AtcEntry = read["AtcEntry"].ToString();
                detalles.SupplCode = read["SupplCode"].ToString();
                detalles.GTSRlvnt = read["GTSRlvnt"].ToString();
                detalles.BaseDisc = read["BaseDisc"].ToString();
                detalles.BaseDiscSc = read["BaseDiscSc"].ToString();
                detalles.BaseDiscFc = read["BaseDiscFc"].ToString();
                detalles.BaseDiscPr = read["BaseDiscPr"].ToString();
                detalles.CreateTS = read["CreateTS"].ToString();
                detalles.UpdateTS = read["UpdateTS"].ToString();
                detalles.SrvTaxRule = read["SrvTaxRule"].ToString();
                detalles.AnnInvDecR = read["AnnInvDecR"].ToString();
                detalles.Supplier = read["Supplier"].ToString();
                detalles.Releaser = read["Releaser"].ToString();
                detalles.Receiver = read["Receiver"].ToString();
                detalles.ToWhsCode = read["ToWhsCode"].ToString();
                detalles.AssetDate = read["AssetDate"].ToString();
                detalles.Requester = read["Requester"].ToString();
                detalles.ReqName = read["ReqName"].ToString();
                detalles.Branch = read["Branch"].ToString();
                detalles.Department = read["Department"].ToString();
                detalles.Email = read["Email"].ToString();
                detalles.Notify = read["Notify"].ToString();
                detalles.ReqType = read["ReqType"].ToString();
                detalles.OriginType = read["OriginType"].ToString();
                detalles.IsReuseNum = read["IsReuseNum"].ToString();
                detalles.IsReuseNFN = read["IsReuseNFN"].ToString();
                detalles.DocDlvry = read["DocDlvry"].ToString();
                detalles.PaidDpm = read["PaidDpm"].ToString();
                detalles.PaidDpmF = read["PaidDpmF"].ToString();
                detalles.PaidDpmS = read["PaidDpmS"].ToString();
                detalles.EnvTypeNFe = read["EnvTypeNFe"].ToString();
                detalles.AgrNo = read["AgrNo"].ToString();
                detalles.IsAlt = read["IsAlt"].ToString();
                detalles.AltBaseTyp = read["AltBaseTyp"].ToString();
                detalles.AltBaseEnt = read["AltBaseEnt"].ToString();
                detalles.AuthCode = read["AuthCode"].ToString();
                detalles.StDlvDate = read["StDlvDate"].ToString();
                detalles.StDlvTime = read["StDlvTime"].ToString();
                detalles.EndDlvDate = read["EndDlvDate"].ToString();
                detalles.EndDlvTime = read["EndDlvTime"].ToString();
                detalles.VclPlate = read["VclPlate"].ToString();
                detalles.ElCoStatus = read["ElCoStatus"].ToString();
                detalles.AtDocType = read["AtDocType"].ToString();
                detalles.ElCoMsg = read["ElCoMsg"].ToString();
                detalles.PrintSEPA = read["PrintSEPA"].ToString();
                detalles.FreeChrg = read["FreeChrg"].ToString();
                detalles.FreeChrgFC = read["FreeChrgFC"].ToString();
                detalles.FreeChrgSC = read["FreeChrgSC"].ToString();
                detalles.NfeValue = read["NfeValue"].ToString();
                detalles.FiscDocNum = read["FiscDocNum"].ToString();
                detalles.RelatedTyp = read["RelatedTyp"].ToString();
                detalles.RelatedEnt = read["RelatedEnt"].ToString();
                detalles.CCDEntry = read["CCDEntry"].ToString();
                detalles.NfePrntFo = read["NfePrntFo"].ToString();
                detalles.ZrdAbs = read["ZrdAbs"].ToString();
                detalles.POSRcptNo = read["POSRcptNo"].ToString();
                detalles.FoCTax = read["FoCTax"].ToString();
                detalles.FoCTaxFC = read["FoCTaxFC"].ToString();
                detalles.FoCTaxSC = read["FoCTaxSC"].ToString();
                detalles.TpCusPres = read["TpCusPres"].ToString();
                detalles.ExcDocDate = read["ExcDocDate"].ToString();
                detalles.FoCFrght = read["FoCFrght"].ToString();
                detalles.FoCFrghtFC = read["FoCFrghtFC"].ToString();
                detalles.FoCFrghtSC = read["FoCFrghtSC"].ToString();
                detalles.InterimTyp = read["InterimTyp"].ToString();
                detalles.PTICode = read["PTICode"].ToString();
                detalles.Letter = read["Letter"].ToString();
                detalles.FolNumFrom = read["FolNumFrom"].ToString();
                detalles.FolNumTo = read["FolNumTo"].ToString();
                detalles.FolSeries = read["FolSeries"].ToString();
                detalles.SplitTax = read["SplitTax"].ToString();
                detalles.SplitTaxFC = read["SplitTaxFC"].ToString();
                detalles.SplitTaxSC = read["SplitTaxSC"].ToString();
                detalles.ToBinCode = read["ToBinCode"].ToString();
                detalles.PriceMode = read["PriceMode"].ToString();
                detalles.PoDropPrss = read["PoDropPrss"].ToString();
                detalles.PermitNo = read["PermitNo"].ToString();
                detalles.MYFtype = read["MYFtype"].ToString();
                detalles.DocTaxID = read["DocTaxID"].ToString();
                detalles.DateReport = read["DateReport"].ToString();
                detalles.RepSection = read["RepSection"].ToString();
                detalles.ExclTaxRep = read["ExclTaxRep"].ToString();
                detalles.PosCashReg = read["PosCashReg"].ToString();
                detalles.DmpTransID = read["DmpTransID"].ToString();
                detalles.ECommerBP = read["ECommerBP"].ToString();
                detalles.EComerGSTN = read["EComerGSTN"].ToString();
                detalles.Revision = read["Revision"].ToString();
                detalles.RevRefNo = read["RevRefNo"].ToString();
                detalles.RevRefDate = read["RevRefDate"].ToString();
                detalles.RevCreRefN = read["RevCreRefN"].ToString();
                detalles.RevCreRefD = read["RevCreRefD"].ToString();
                detalles.TaxInvNo = read["TaxInvNo"].ToString();
                detalles.FrmBpDate = read["FrmBpDate"].ToString();
                detalles.GSTTranTyp = read["GSTTranTyp"].ToString();
                detalles.BaseType = read["BaseType"].ToString();
                detalles.BaseEntry = read["BaseEntry"].ToString();
                detalles.ComTrade = read["ComTrade"].ToString();
                detalles.UseBilAddr = read["UseBilAddr"].ToString();
                detalles.IssReason = read["IssReason"].ToString();
                detalles.ComTradeRt = read["ComTradeRt"].ToString();
                detalles.SplitPmnt = read["SplitPmnt"].ToString();
                detalles.SOIWizId = read["SOIWizId"].ToString();
            }
            read.Close();
            conexion.Close();
            return detalles;
        }
    }


}