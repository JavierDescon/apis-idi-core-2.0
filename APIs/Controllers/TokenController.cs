﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using APIs.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace APIs.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        private IConfiguration _config;
        List<Login> listaUsuarios;

        public TokenController(IConfiguration config)
        {
            _config = config;
            listaUsuarios = new List<Login>();

            //Usuarios con acceso, para generacion de token
            listaUsuarios.Add(new Login( "Javier", "jav3097."));
            listaUsuarios.Add(new Login( "Claudia", "Admin123"));
        }


        [AllowAnonymous]
        [HttpPost]
        public IActionResult crearToken([FromBody]Login usuario)//Se manda un Json de tipo Login 
        {
            IActionResult response = Unauthorized();
            
            if (Authenticate(usuario))//Condicion de autenticacion del usuario
            {
                var tokenString = BuildToken(); //Se invocara el metodo para construccion del token
                response = Ok(new { token = tokenString });
            }
            else
            {
                response = Ok(new { token = "Usuario y/o contraseña incorrecta" });
;            }
            return response; //Se retornara el TOKEN de acceso
        }

        private bool Authenticate(Login usuario)
        {
            var user = false;
            var use = listaUsuarios.SingleOrDefault(x => x.Username == usuario.Username && x.Password == usuario.Password); //Verificamos si le usuario y contraseña existen
            if (use != null)
            {
                user = true; //En caso de existir retornara TRUE
            }
            return user;//Retornamos el resultado
        }
        private string BuildToken()
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
              _config["Jwt:Issuer"],
              expires: DateTime.Now.AddDays(1),
              signingCredentials: creds);
            return new JwtSecurityTokenHandler().WriteToken(token); //Se mandara el TOKEN construido por la Web API
        }
    }
}