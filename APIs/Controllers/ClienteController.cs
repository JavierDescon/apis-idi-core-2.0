﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using APIs.Models;

namespace APIs.Controllers
{
    [Authorize]//Solicitar Autorizacion de JWT
    [Route("api/[controller]")]
    [ApiController]
    public class ClienteController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        List<Cliente> listaClientes = new List<Cliente>();
        public ClienteController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        [HttpGet]
        public List<Cliente> getClientes() //se filtran por CardType ="C" y nombre de carta !=NULL
        {
            string server = _configuration["Configuration:server"];
            string user = _configuration["Configuration:user"];
            string bd = _configuration["Configuration:database"];
            string pass = _configuration["Configuration:pass"];

            SqlCommand query = new SqlCommand("SELECT CardCode,CardName,CardType FROM OCRD WHERE CardType = 'C' and CardName IS NOT NULL;", ConexionBd.Conectar(server, bd, user, pass));//Query para generar consulta
            SqlDataReader read = query.ExecuteReader();
            while (read.Read()) //Se agregan todos y cada uno de los registros obtenidos a la lista clientes
            {
                Cliente clienteTemp = new Cliente();
                clienteTemp.cardCode = read[0].ToString();
                clienteTemp.cardName = read[1].ToString();
                clienteTemp.cardType = read[2].ToString(); ;
                clienteTemp.listaArticulos = getArticulosCliente(clienteTemp);
                listaClientes.Add(clienteTemp);
            }
            return listaClientes; //Retornamos la lista clientes
        }

        // GET api/values

        [HttpPost]
        public IEnumerable<Cliente> getCliente([FromHeader]int DocEntry) { //Metodo GET con parametro como filtro de consulta donde =>   DocEntry=@param1
            
            string server = _configuration["Configuration:server"];
            string user = _configuration["Configuration:user"];
            string bd = _configuration["Configuration:database"];
            string pass = _configuration["Configuration:pass"];
           
            SqlCommand query = new SqlCommand("SELECT CardCode,CardName,CardType FROM OCRD WHERE CardType = 'C' and CardName IS NOT NULL and DocEntry =" + DocEntry + ";", ConexionBd.Conectar(server, bd, user, pass));
            SqlDataReader read = query.ExecuteReader();
            if (read.Read())
            {
                Cliente clienteTemp = new Cliente();
                clienteTemp = new Cliente();
                clienteTemp.cardCode = read[0].ToString();
                clienteTemp.cardName = read[1].ToString();
                clienteTemp.cardType = read[2].ToString(); ;
                clienteTemp.listaArticulos = getArticulosCliente(clienteTemp);
                listaClientes.Add(clienteTemp);
            }
            return listaClientes;
        }

        [HttpGet("{tipoVendedor}")]
        public IEnumerable<string> getClientes(string TipoVendedor)
        {
            List<String> clientes = new List<string>();
            string server = _configuration["Configuration:server"];
            string user = _configuration["Configuration:user"];
            string bd = _configuration["Configuration:database"];
            string pass = _configuration["Configuration:pass"];

            SqlCommand query = new SqlCommand("select (T0.CardCode+ ' '+T1.CardName +'/ ' + t0.Address) " +
                "from crd1 T0 inner join OCRD T1 " +
                "on T0.CardCode = T1.CardCode " +
                "INNER JOIN OSLP ON t0.U_SlpCode = OSLP.SlpCode " +
                "WHERE OSLP.U_IDI_TipoEj ='" + TipoVendedor + "';", ConexionBd.Conectar(server, bd, user, pass));
            SqlDataReader read = query.ExecuteReader();
            if (read.Read())
            {
                clientes.Add(read[0].ToString());
            }
            return clientes;
        }

        //Obtener articulos por cliente
        private List<Articulos> getArticulosCliente(Cliente cliente){
       
            string server = _configuration["Configuration:server"];
            string user = _configuration["Configuration:user"];
            string bd = _configuration["Configuration:database"];
            string pass = _configuration["Configuration:pass"];
            SqlConnection conexion = ConexionBd.Conectar(server, bd, user, pass);

            List<Articulos> listaArticulos = new List<Articulos>() ;
            SqlCommand query = new SqlCommand("select * from ospp where CardCode = '"+cliente.cardCode + "'; ", conexion);
            SqlDataReader read = query.ExecuteReader();
            while (read.Read())
            {
                Articulos articuloTemp = new Articulos();
                articuloTemp.ItemCode = read[0].ToString();
                articuloTemp.CardCode = read[1].ToString();
                articuloTemp.Price = read[2].ToString();
                articuloTemp.Currency = read[3].ToString();
                articuloTemp.Discount = read[4].ToString();
                articuloTemp.ListNum = read[5].ToString();
                articuloTemp.AutoUpdt = read[6].ToString();
                articuloTemp.EXPAND = read[7].ToString();
                articuloTemp.UserSign = read[8].ToString();
                articuloTemp.SrcPrice = read[9].ToString();
                articuloTemp.LogInstanc = read[10].ToString();
                articuloTemp.UserSign2 = read[11].ToString();
                articuloTemp.CreateDate = read[12].ToString();
                articuloTemp.UpdateDate = read[13].ToString();
                articuloTemp.Valid = read[14].ToString();
                articuloTemp.ValidFrom = read[15].ToString();
                articuloTemp.ValidTo = read[16].ToString();
                listaArticulos.Add(articuloTemp);
            }
            conexion.Close();
            return listaArticulos;
        }
    }
}