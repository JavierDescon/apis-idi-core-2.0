﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using APIs.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Net.Mime;
using System.IO;

//using  System.Net.Mime;

namespace APIs.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]

    public class ArticulosController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        public ArticulosController(IConfiguration configuration) {
            _configuration = configuration;
        }
        public IEnumerable<Articulos> get() {

            string server = _configuration["Configuration:server"];
            string user = _configuration["Configuration:user"];
            string bd = _configuration["Configuration:database"];
            string pass = _configuration["Configuration:pass"];
            
            List<Articulos> listaArticulos = new List<Articulos>();
            SqlCommand query = new SqlCommand("select * from ospp;", ConexionBd.Conectar(server, bd, user, pass));
            SqlDataReader read = query.ExecuteReader();
            while (read.Read())
            {
                Articulos articuloTemp = new Articulos();
                articuloTemp.ItemCode = read[0].ToString();
                articuloTemp.CardCode = read[1].ToString();
                articuloTemp.Price = read[2].ToString();
                articuloTemp.Currency = read[3].ToString();
                articuloTemp.Discount = read[4].ToString();
                articuloTemp.ListNum = read[5].ToString();
                articuloTemp.AutoUpdt = read[6].ToString();
                articuloTemp.EXPAND = read[7].ToString();
                articuloTemp.UserSign = read[8].ToString();
                articuloTemp.SrcPrice = read[9].ToString();
                articuloTemp.LogInstanc = read[10].ToString();
                articuloTemp.UserSign2 = read[11].ToString();
                articuloTemp.CreateDate = read[12].ToString();
                articuloTemp.UpdateDate = read[13].ToString();
                articuloTemp.Valid = read[14].ToString();
                articuloTemp.ValidFrom = read[15].ToString();
                articuloTemp.ValidTo = read[16].ToString();
                articuloTemp.foto = EncodeImage(articuloTemp.ItemCode);
                listaArticulos.Add(articuloTemp);
            }
            return listaArticulos;
        }
        [HttpGet("{cardCode}")]
        public IEnumerable<Articulos> getArticulos(string cardCode) //filra articulos por vendededor
        {
            string server = _configuration["Configuration:server"];
            string user = _configuration["Configuration:user"];
            string bd = _configuration["Configuration:database"];
            string pass = _configuration["Configuration:pass"];

            List<Articulos> listaArticulos = new List<Articulos>();
            SqlCommand query = new SqlCommand("select * " +
                "from ospp " +
                "where CardCode = '"+cardCode +"';", ConexionBd.Conectar(server, bd, user, pass));
            SqlDataReader read = query.ExecuteReader();
            while (read.Read())
            {
                Articulos articuloTemp = new Articulos();
                articuloTemp.ItemCode = read[0].ToString();
                articuloTemp.CardCode = read[1].ToString();
                articuloTemp.Price = read[2].ToString();
                articuloTemp.Currency = read[3].ToString();
                articuloTemp.Discount = read[4].ToString();
                articuloTemp.ListNum = read[5].ToString();
                articuloTemp.AutoUpdt = read[6].ToString();
                articuloTemp.EXPAND = read[7].ToString();
                articuloTemp.UserSign = read[8].ToString();
                articuloTemp.SrcPrice = read[9].ToString();
                articuloTemp.LogInstanc = read[10].ToString();
                articuloTemp.UserSign2 = read[11].ToString();
                articuloTemp.CreateDate = read[12].ToString();
                articuloTemp.UpdateDate = read[13].ToString();
                articuloTemp.Valid = read[14].ToString();
                articuloTemp.ValidFrom = read[15].ToString();
                articuloTemp.ValidTo = read[16].ToString();
                listaArticulos.Add(articuloTemp);
            }
            return listaArticulos;
        }

        [HttpPost]
        public IEnumerable<Descuento> getPrecioArticulos([FromHeader]string ItemCode, [FromHeader]string CardCode) {
            string server = _configuration["Configuration:server"];
            string user = _configuration["Configuration:user"];
            string bd = _configuration["Configuration:database"];
            string pass = _configuration["Configuration:pass"];

            List<Descuento> listaArticulos = new List<Descuento>();
            SqlCommand query = new SqlCommand("select t1.Price , Discount ,  (T1.Price *  (100-T0.Discount)/100) [Precio Tras Descuento], t0.ItemCode  " +
                "from OSPP T0 Inner Join ITM1 T1 oN T0.ItemCode = T1.ItemCode " +
                "wHERE t0.ItemCode = '"+ItemCode +"' and PriceList = '2' and T0.CardCode = '"+CardCode +"'; ", ConexionBd.Conectar(server, bd, user, pass));
            SqlDataReader read = query.ExecuteReader();
            while (read.Read())
            {
                Descuento descuentoTemp = new Descuento();
                descuentoTemp.itemCode = read[3].ToString();
                descuentoTemp.precio = read[0].ToString() ;
                descuentoTemp.descuento = read[1].ToString();
                descuentoTemp.precio_tras_descuento = read[2].ToString();
            }
            return listaArticulos;
        }



        /// 
        ///	  Codifica una imagen en Base64
        /// 
        private string EncodeImage(string nombreImagen)
        {
            string ruta = _configuration["Configuration:RutaImagenes"];
            try
            {
                byte[] imageArray = System.IO.File.ReadAllBytes(ruta+ "\\" + nombreImagen+".jpg");
                return Convert.ToBase64String(imageArray);
            }
            catch (Exception)
            {

                return "Foto no disponible";
            }
            
        }
    }

}