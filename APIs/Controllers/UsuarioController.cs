﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using APIs.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;

namespace APIs.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        List<Usuario> listaUsuario = new List<Usuario>();
        public UsuarioController(IConfiguration configuration)
        {
            _configuration = configuration;
           
        }

        [HttpGet]
        public List<Usuario> getUsuarios()
        {
            string server = _configuration["Configuration:server"];
            string user = _configuration["Configuration:user"];
            string bd = _configuration["Configuration:database"];
            string pass = _configuration["Configuration:pass"];
            SqlCommand query = new SqlCommand("SELECT SlpCode,SlpName FROM OSLP;", ConexionBd.Conectar(server, bd, user, pass));
            //SqlCommand query = new SqlCommand("SELECT SlpCode,SlpName,U_UsuarioWeb,U_PassWeb FROM OSLP;", ConexionBd.Conectar(server, bd, user, pass));
            SqlDataReader read = query.ExecuteReader();
            while (read.Read())
            {
                Usuario usuarioTemp = new Usuario();
                usuarioTemp.SlpCode = read[0].ToString();
                usuarioTemp.SLPName = read[1].ToString();
                //usuarioTemp.UsuarioWeb=read[2].ToString();
                //usuarioTemp.PassWeb= read[3].ToString();
                usuarioTemp.listaClientes = getClientes(usuarioTemp);
                listaUsuario.Add(usuarioTemp);
            }
            return listaUsuario;
        }

        [HttpGet("{slpCode}")]
        public List<string> getUsuario(string SlpCode)
        {
            string server = _configuration["Configuration:server"];
            string user = _configuration["Configuration:user"];
            string bd = _configuration["Configuration:database"];
            string pass = _configuration["Configuration:pass"];
            List<String> consultas = new List<string>();
           
            SqlCommand query = new SqlCommand("select (T0.CardCode+ ' '+T1.CardName +'/ ' + t0.Address) " +
                "from crd1 T0 inner join OCRD T1 on T0.CardCode = T1.CardCode " +
                "where SlpCode = '"+SlpCode +"'; ", ConexionBd.Conectar(server, bd, user, pass));
            //SqlCommand query = new SqlCommand("SELECT SlpCode,SlpName,U_UsuarioWeb,U_PassWeb FROM OSLP;", ConexionBd.Conectar(server, bd, user, pass));
            SqlDataReader read = query.ExecuteReader();
            while (read.Read())
            {
                consultas.Add(read[0].ToString());
            }
            if (consultas.Count == 0)
                consultas.Add("No hay datos para mostrar");
            return consultas;
        }

        //[HttpGet("{Parametro}")]
        //public Usuario getUsuario(string SlpCode)
        //{
        //    string server = _configuration["Configuration:server"];
        //    string user = _configuration["Configuration:user"];
        //    string bd = _configuration["Configuration:database"];
        //    string pass = _configuration["Configuration:pass"];
        //    Usuario usuarioTemp = new Usuario();
        //    SqlCommand query = new SqlCommand("SELECT SlpCode,SlpName FROM OSLP WHERE SLPCode='"+SlpCode+"';", ConexionBd.Conectar(server, bd, user, pass));
        //    SqlDataReader read = query.ExecuteReader();
        //    while (read.Read())
        //    {
               
        //        usuarioTemp.SlpCode = read[0].ToString();
        //        usuarioTemp.SLPName = read[1].ToString();
        //        usuarioTemp.listaClientes = getClientes(usuarioTemp);
        //        listaUsuario.Add(usuarioTemp);
        //    }
        //    return usuarioTemp;
        //}

        private List<Cliente> getClientes(Usuario usuario)
        {
            string server = _configuration["Configuration:server"];
            string user = _configuration["Configuration:user"];
            string bd = _configuration["Configuration:database"];
            string pass = _configuration["Configuration:pass"];
            SqlConnection conexion = ConexionBd.Conectar(server, bd, user, pass);

            List<Cliente> listaClientes = new List<Cliente>();
            SqlCommand query = new SqlCommand("  SELECT * FROM OCRD Where SlpCode = '" + usuario.SlpCode + "'; ", conexion);
            SqlDataReader read = query.ExecuteReader();
            while (read.Read()) //Se agregan todos y cada uno de los registros obtenidos a la lista clientes
            {
                Cliente clienteTemp = new Cliente();
                clienteTemp.cardCode = read["CardCode"].ToString();
                clienteTemp.cardName = read["CardName"].ToString();
                clienteTemp.cardType = read["CardType"].ToString();
                listaClientes.Add(clienteTemp);
            }
            read.Close();
            conexion.Close();
            return listaClientes; //Retornamos la lista clientes
        }
    }
}