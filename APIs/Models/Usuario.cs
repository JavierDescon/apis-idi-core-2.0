﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIs.Models
{
    public class Usuario
    {
        public string UsuarioWeb;
        public string PassWeb;
        public string SlpCode { get; set; }
        public string SLPName { get; set; }
        public List<Cliente> listaClientes;

        public Usuario() { }
    }
}
