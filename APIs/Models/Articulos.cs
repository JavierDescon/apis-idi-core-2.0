﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIs.Models
{
    public class Articulos
    {
        public string Field;
        public string ItemCode;
        public string CardCode;
        public string Price;
        public string Currency;
        public string Discount;
        public string ListNum;
        public string AutoUpdt;
        public string EXPAND;
        public string UserSign;
        public string SrcPrice;
        public string LogInstanc;
        public string UserSign2;
        public string CreateDate;
        public string UpdateDate;
        public string Valid;
        public string ValidFrom;
        public string ValidTo;
        public string foto;
    }
    public class Descuento{
        public string precio;
        public string itemCode;
        public string descuento;
        public string precio_tras_descuento;
    }
}
